
import java.util.Scanner;
import java.util.Random;

public class RaumschiffAbfragen {
	public static void main(String[] args) {
		 int abfrage;
		Scanner tastatur = new Scanner(System.in);
		
		//Erstellung der Raumschiffe und deren Ladungen
		Raumschiff klingonen = new Raumschiff("IKS Hengh'ta", 100, 100, 100, 100, 1, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 50, 100, 0, 5);

		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));		
		
		do {
		
		//Auswahl des Raumschiffs, mit dem eine Aktion ausgef�hrt werden soll
		System.out.println("Welches Raumschiff m�chten Sie answ�hlen?");
		System.out.println("1 - " + klingonen.getSchiffsname());
		System.out.println("2 - " + romulaner.getSchiffsname());
		System.out.println("3 - " + vulkanier.getSchiffsname());
		System.out.println("4 - Zustand aller Raumschiffe abrufen");

		switch (tastatur.nextInt()) {
		
		//Auswahl: Raumschiff der Klingonen
		case 1:
			//Auswahl der Aktion
			System.out.println("\n\nWas m�chten Sie tun?");
			System.out.println("1 - Photonentorpedos abschie�en");
			System.out.println("2 - Phaserkanone abschie�en");
			System.out.println("3 - Zustand und Ladungsverzeichnis abrufen");
			System.out.println("4 - Nachricht an Alle");
			System.out.println("5 - Broadcast Kommunikator ausgeben");
			System.out.println("6 - Zur�ck");
			switch (tastatur.nextInt()) {
			//Auswahl: Photonentorpedos abschie�en
			case 1:
				//�berpr�fung, ob Photonentorpedos geladen sind
				if (klingonen.getPhotonentorpedoAnzahl() == 0) {
					System.out.println("-=*Click*=-");
					break;
				} else {
					int test = photonentorpedosAbschiessen();
					klingonen.setPhotonentorpedoAnzahl(klingonen.getPhotonentorpedoAnzahl() - 1);
					int n =treffer(test);
					if (n != 0) {
						switch (test) {
						case 1:
							System.out.println("IKS Hengh'ta, Raumschiff der Klingonen wurde getroffen!");
							klingonen.setSchildeInProzent(klingonen.getSchildeInProzent()-50);
							break;
						case 2:
							System.out.println("IRW Khazara, Raumschiff der Romulaner wurde getroffen!");
							romulaner.setSchildeInProzent(romulaner.getSchildeInProzent()-50);
							break;
						case 3:
							System.out.println("Ni'Var, Raumschiff der Vulkanier wurde getroffen!");
							vulkanier.setSchildeInProzent(vulkanier.getSchildeInProzent()-50);
							break;
						}
					}
					break;
				}
			//Auswahl: Phaserkanone abschie�en
			case 2:
				//�berpr�fung, ob Energie vorhanden ist
				if (klingonen.getEnergieversorgungInProzent() < 50) {
					System.out.println("-=*Click*=-");
					break;
				} else {
					int test = phaserkanoneAbschiessen();
					klingonen.setEnergieversorgungInProzent(klingonen.getEnergieversorgungInProzent() - 50);
					int n =treffer(test);
					if (n != 0) {
						switch (test) {
						case 1:
							System.out.println("IKS Hengh'ta, Raumschiff der Klingonen wurde getroffen!");
							klingonen.setSchildeInProzent(klingonen.getSchildeInProzent()-50);
							break;
						case 2:
							System.out.println("IRW Khazara, Raumschiff der Romulaner wurde getroffen!");
							romulaner.setSchildeInProzent(romulaner.getSchildeInProzent()-50);
							break;
						case 3:
							System.out.println("Ni'Var, Raumschiff der Vulkanier wurde getroffen!");
							vulkanier.setSchildeInProzent(vulkanier.getSchildeInProzent()-50);
							break;
						}
					}
				}
			case 3:
				klingonen.zustand();
				break;
			case 4:
				System.out.println("Geben Sie eine Nachricht ein, die an alle gesendet werden soll.");
				klingonen.nachrichtAnAlle(tastatur.next());
				break;
			case 5: 
				klingonen.ausgabeBroadcastKommunikator();
				break;
			case 6:
				break;
			}
			break;
		//Auswahl: Raumschiff der Romulaner
		case 2:
			System.out.println("\n\nWas m�chten Sie tun?");
			System.out.println("1 - Photonentorpedos abschie�en");
			System.out.println("2 - Phaserkanone abschie�en");
			System.out.println("3 - Zustand und Ladungsverzeichnis abrufen");
			System.out.println("4 - Nachricht an Alle");
			System.out.println("5 - Broadcast Kommunikator ausgeben");
			System.out.println("6 - Zur�ck");
			switch (tastatur.nextInt()) {
			case 1:
				if (romulaner.getPhotonentorpedoAnzahl() == 0) {
					System.out.println("-=*Click*=-");
					break;
				} else {
					int test = photonentorpedosAbschiessen();
					romulaner.setPhotonentorpedoAnzahl(romulaner.getPhotonentorpedoAnzahl() - 1);
					int n =treffer(test);
					if (n != 0) {
						switch (test) {
						case 1:
							System.out.println("IKS Hengh'ta, Raumschiff der Klingonen wurde getroffen!");
							klingonen.setSchildeInProzent(klingonen.getSchildeInProzent()-50);
							break;
						case 2:
							System.out.println("IRW Khazara, Raumschiff der Romulaner wurde getroffen!");
							romulaner.setSchildeInProzent(romulaner.getSchildeInProzent()-50);
							break;
						case 3:
							System.out.println("Ni'Var, Raumschiff der Vulkanier wurde getroffen!");
							vulkanier.setSchildeInProzent(vulkanier.getSchildeInProzent()-50);
							break;
						}
					}
					break;
				}

			case 2:
				if (romulaner.getEnergieversorgungInProzent() < 50) {
					System.out.println("-=*Click*=-");
					break;
				} else {
					int test = phaserkanoneAbschiessen();
					romulaner.setEnergieversorgungInProzent(romulaner.getEnergieversorgungInProzent() - 50);
					int n =treffer(test);
					if (n != 0) {
						switch (test) {
						case 1:
							System.out.println("IKS Hengh'ta, Raumschiff der Klingonen wurde getroffen!");
							klingonen.setSchildeInProzent(klingonen.getSchildeInProzent()-50);
							break;
						case 2:
							System.out.println("IRW Khazara, Raumschiff der Romulaner wurde getroffen!");
							romulaner.setSchildeInProzent(romulaner.getSchildeInProzent()-50);
							break;
						case 3:
							System.out.println("Ni'Var, Raumschiff der Vulkanier wurde getroffen!");
							vulkanier.setSchildeInProzent(vulkanier.getSchildeInProzent()-50);
							break;
						}
					}
				}
			case 3:
				romulaner.zustand();
				break;
			case 4:
				System.out.println("Geben Sie eine Nachricht ein, die an alle gesendet werden soll.");
				String testeingabe = tastatur.next();
				System.out.println(testeingabe);
//				romulaner.nachrichtAnAlle(tastatur.nextLine());
				//break;
			case 5: 
				romulaner.ausgabeBroadcastKommunikator();
				break;
			case 6:
				break;
			}
			break;
		//Auswahl: Raumschiff der Vulkanier
		case 3:
			System.out.println("\n\nWas m�chten Sie tun?");
			System.out.println("1 - Photonentorpedos abschie�en");
			System.out.println("2 - Phaserkanone abschie�en");
			System.out.println("3 - Zustand und Ladungsverzeichnis abrufen");
			System.out.println("4 - Nachricht an Alle");
			System.out.println("5 - Broadcast Kommunikator ausgeben");
			System.out.println("6 - Zur�ck");
			switch (tastatur.nextInt()) {
			case 1:
				if (vulkanier.getPhotonentorpedoAnzahl() == 0) {
					System.out.println("-=*Click*=-");
					break;
				} else {
					int test = photonentorpedosAbschiessen();
					vulkanier.setPhotonentorpedoAnzahl(vulkanier.getPhotonentorpedoAnzahl() - 1);
					int n =treffer(test);
					if (n != 0) {
						switch (test) {
						case 1:
							System.out.println("IKS Hengh'ta, Raumschiff der Klingonen wurde getroffen!");
							klingonen.setSchildeInProzent(klingonen.getSchildeInProzent()-50);
							break;
						case 2:
							System.out.println("IRW Khazara, Raumschiff der Romulaner wurde getroffen!");
							romulaner.setSchildeInProzent(romulaner.getSchildeInProzent()-50);
							break;
						case 3:
							System.out.println("Ni'Var, Raumschiff der Vulkanier wurde getroffen!");
							vulkanier.setSchildeInProzent(vulkanier.getSchildeInProzent()-50);
							break;
						}
					}
				}

			case 2:
				if (vulkanier.getEnergieversorgungInProzent() < 50) {
					System.out.println("-=*Click*=-");
					break;
				} else {
					int test = phaserkanoneAbschiessen();
					vulkanier.setEnergieversorgungInProzent(vulkanier.getEnergieversorgungInProzent() - 50);
					int n =treffer(test);
					if (n != 0) {
						switch (test) {
						case 1:
							System.out.println("IKS Hengh'ta, Raumschiff der Klingonen wurde getroffen!");
							klingonen.setSchildeInProzent(klingonen.getSchildeInProzent()-50);
							break;
						case 2:
							System.out.println("IRW Khazara, Raumschiff der Romulaner wurde getroffen!");
							romulaner.setSchildeInProzent(romulaner.getSchildeInProzent()-50);
							break;
						case 3:
							System.out.println("Ni'Var, Raumschiff der Vulkanier wurde getroffen!");
							vulkanier.setSchildeInProzent(vulkanier.getSchildeInProzent()-50);
							break;
						}
					}
				}
			//
			case 3:
				vulkanier.zustand();
				break;
			case 4:
				System.out.println("Geben Sie eine Nachricht ein, die an alle gesendet werden soll.");
				vulkanier.nachrichtAnAlle(tastatur.next());
				break;
			case 5: 
				vulkanier.ausgabeBroadcastKommunikator();
				break;
			case 6:
				break;
			}
			break;
		//Zustand aller Raumschiffe ausgeben
		case 4:			
			System.out.println("-------------------------------");
			System.out.println("Raumschiff der Klingonen");
			klingonen.zustand();
			System.out.println("-------------------------------");
			System.out.println("Raumschiff der Romulaner");
			romulaner.zustand();
			System.out.println("-------------------------------");
			System.out.println("Raumschiff der Vulkanier");
			vulkanier.zustand();
		}
		System.out.println("M�chten Sie weitere Aktionen ausf�hren?\n 1 - Ja\n 2 - Nein");
		abfrage = tastatur.nextInt();
	} while (abfrage == 1);
	}
	
	// Photonentorpedos abschie�en
	public static int photonentorpedosAbschiessen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Auf welches Raumschiff soll geschossen werden?");
		System.out.println("1 - IKS Hengh'ta");
		System.out.println("2 - IRW Khazara");
		System.out.println("3 - Ni'Var");

		int raumschiffNr = tastatur.nextInt();
		return raumschiffNr;
//		System.out.println("Photonentorpedo abgeschossen");
//		treffer(raumschiffNr);
	}

	// Phaserkanone abschie�en
	public static int phaserkanoneAbschiessen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Auf welches Raumschiff soll geschossen werden?");
		System.out.println("1 - IKS Hengh'ta");
		System.out.println("2 - IRW Khazara");
		System.out.println("3 - Ni'Var");

		int raumschiffNr = tastatur.nextInt();
		return raumschiffNr;
//		System.out.println("Phaserkanone abgeschossen");
//		treffer(raumschiffNr);
	}
	
	// Treffer
	public static int treffer(int raumschiffNr) {
		Random rand = new Random();
		int n = rand.nextInt(2);
		if (n == 0) {
			System.out.println("Kein Treffer.");
			return n;
		} else {
			return n;
		}
	}
}
