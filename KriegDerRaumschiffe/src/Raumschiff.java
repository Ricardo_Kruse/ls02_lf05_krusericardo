import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private double energieversorgungInProzent;
	private double schildeInProzent;
	private double huelleInProzent;
	private double lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	public Raumschiff() {
	}

	public Raumschiff(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public Raumschiff(String schiffsname, double energieversorgungInProzent) {
		this(schiffsname);
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public Raumschiff(String schiffsname, double energieversorgungInProzent, double schildeInProzent) {
		this(schiffsname);
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
	}

	public Raumschiff(String schiffsname, double energieversorgungInProzent, double schildeInProzent,
			double huelleInProzent) {
		this(schiffsname);
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
	}

	public Raumschiff(String schiffsname, double energieversorgungInProzent, double schildeInProzent,
			double huelleInProzent, double lebenserhaltungssystemeInProzent) {
		this(schiffsname);
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public Raumschiff(String schiffsname, double energieversorgungInProzent, double schildeInProzent,
			double huelleInProzent, double lebenserhaltungssystemeInProzent, int photonentorpedoAnzahl) {
		this(schiffsname);
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public Raumschiff(String schiffsname, double energieversorgungInProzent, double schildeInProzent,
			double huelleInProzent, double lebenserhaltungssystemeInProzent, int photonentorpedoAnzahl,
			int androidenAnzahl) {
		this(schiffsname);
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.androidenAnzahl = androidenAnzahl;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public String getSchiffsname() {
		return this.schiffsname;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
			this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}

	public void setEnergieversorgungInProzent(double energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public double getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}

	public void setSchildeInProzent(double schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public double getSchildeInProzent() {
		return this.schildeInProzent;
	}

	public void setHuelleInProzent(double huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public double getHuelleInProzent() {
		return this.huelleInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(double lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public double getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
			this.androidenAnzahl = androidenAnzahl;
	}

	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	
	public void addLadung (Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	
	public void addMessageBroadcastkommunikator(String message) {
		this.broadcastKommunikator.add(message);
	}
	
	public void ausgabeBroadcastKommunikator() {
		System.out.println("\nBroadcast Kommunikator: ");
		for(int i=0; i< broadcastKommunikator.size(); i++) {
			System.out.println(broadcastKommunikator.get(i));
		}
	}
	
	public void ausgabeLadungsverzeichnis() {
		System.out.println("\nLadung des Raumschiffes: ");
		for(int i=0;i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i).getBezeichnung() + ": " + ladungsverzeichnis.get(i).getMenge());
		}		
	}
	public void zustand() {
		System.out.println("Schiffsname: " + getSchiffsname());
		System.out.println("Energieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + getSchildeInProzent() + "%");
		System.out.println("Lebenserhaltungssysteme: " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("H�lle: " + getHuelleInProzent() + "%");
		System.out.println("Anzahl der Reperaturandroiden: " + getAndroidenAnzahl());
		System.out.println("Anzahl der Photonentorpedos: " + getPhotonentorpedoAnzahl());
		System.out.println("-------------------------------");
		System.out.println("Ladungsverzeichnis");
		ausgabeLadungsverzeichnis();
		System.out.println("-------------------------------");
	}
	public void nachrichtAnAlle (String nachricht) {
		System.out.println("[" + schiffsname + "]: " + nachricht);
		addMessageBroadcastkommunikator (schiffsname + " - " + nachricht);
	}
}