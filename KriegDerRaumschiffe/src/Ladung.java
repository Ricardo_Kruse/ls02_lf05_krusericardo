
public class Ladung {
	private String bezeichnung;
	private int menge;
	
	public Ladung()
	{ }
	public Ladung(String bezeichnung)
	{
		this.bezeichnung = bezeichnung;
	}
	public Ladung(String bezeichnung, int menge)
	{
		this(bezeichnung);
		this.menge = menge;
	}
	
	public void setBezeichnung(String bezeichnung)
	{
		this.bezeichnung = bezeichnung;
	}
	public String getBezeichnung()
	{
		return this.bezeichnung;
	}
	
	public void setMenge(int menge)
	{
		this.menge = menge;
	}
	public int getMenge()
	{
		return this.menge;
	}
	
}
